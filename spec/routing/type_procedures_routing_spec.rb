require "spec_helper"

describe TypeProceduresController do
  describe "routing" do

    it "routes to #index" do
      get("/type_procedures").should route_to("type_procedures#index")
    end

    it "routes to #new" do
      get("/type_procedures/new").should route_to("type_procedures#new")
    end

    it "routes to #show" do
      get("/type_procedures/1").should route_to("type_procedures#show", :id => "1")
    end

    it "routes to #edit" do
      get("/type_procedures/1/edit").should route_to("type_procedures#edit", :id => "1")
    end

    it "routes to #create" do
      post("/type_procedures").should route_to("type_procedures#create")
    end

    it "routes to #update" do
      put("/type_procedures/1").should route_to("type_procedures#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/type_procedures/1").should route_to("type_procedures#destroy", :id => "1")
    end

  end
end

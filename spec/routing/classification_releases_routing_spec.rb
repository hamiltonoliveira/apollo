require "spec_helper"

describe ClassificationReleasesController do
  describe "routing" do

    it "routes to #index" do
      get("/classification_releases").should route_to("classification_releases#index")
    end

    it "routes to #new" do
      get("/classification_releases/new").should route_to("classification_releases#new")
    end

    it "routes to #show" do
      get("/classification_releases/1").should route_to("classification_releases#show", :id => "1")
    end

    it "routes to #edit" do
      get("/classification_releases/1/edit").should route_to("classification_releases#edit", :id => "1")
    end

    it "routes to #create" do
      post("/classification_releases").should route_to("classification_releases#create")
    end

    it "routes to #update" do
      put("/classification_releases/1").should route_to("classification_releases#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/classification_releases/1").should route_to("classification_releases#destroy", :id => "1")
    end

  end
end

require "spec_helper"

describe DentistsController do
  describe "routing" do

    it "routes to #index" do
      get("/dentists").should route_to("dentists#index")
    end

    it "routes to #new" do
      get("/dentists/new").should route_to("dentists#new")
    end

    it "routes to #show" do
      get("/dentists/1").should route_to("dentists#show", :id => "1")
    end

    it "routes to #edit" do
      get("/dentists/1/edit").should route_to("dentists#edit", :id => "1")
    end

    it "routes to #create" do
      post("/dentists").should route_to("dentists#create")
    end

    it "routes to #update" do
      put("/dentists/1").should route_to("dentists#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/dentists/1").should route_to("dentists#destroy", :id => "1")
    end

  end
end

require "spec_helper"

describe ApportionmentsController do
  describe "routing" do

    it "routes to #index" do
      get("/apportionments").should route_to("apportionments#index")
    end

    it "routes to #new" do
      get("/apportionments/new").should route_to("apportionments#new")
    end

    it "routes to #show" do
      get("/apportionments/1").should route_to("apportionments#show", :id => "1")
    end

    it "routes to #edit" do
      get("/apportionments/1/edit").should route_to("apportionments#edit", :id => "1")
    end

    it "routes to #create" do
      post("/apportionments").should route_to("apportionments#create")
    end

    it "routes to #update" do
      put("/apportionments/1").should route_to("apportionments#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/apportionments/1").should route_to("apportionments#destroy", :id => "1")
    end

  end
end

require "spec_helper"

describe FinancialReleasesController do
  describe "routing" do

    it "routes to #index" do
      get("/financial_releases").should route_to("financial_releases#index")
    end

    it "routes to #new" do
      get("/financial_releases/new").should route_to("financial_releases#new")
    end

    it "routes to #show" do
      get("/financial_releases/1").should route_to("financial_releases#show", :id => "1")
    end

    it "routes to #edit" do
      get("/financial_releases/1/edit").should route_to("financial_releases#edit", :id => "1")
    end

    it "routes to #create" do
      post("/financial_releases").should route_to("financial_releases#create")
    end

    it "routes to #update" do
      put("/financial_releases/1").should route_to("financial_releases#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/financial_releases/1").should route_to("financial_releases#destroy", :id => "1")
    end

  end
end

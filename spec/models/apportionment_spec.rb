require 'spec_helper'

describe Apportionment do
  before do
    FactoryGirl.create(:apportionment_all)
    FactoryGirl.create(:apportionment_dentist)
    FactoryGirl.create(:apportionment_type_procedure)
    FactoryGirl.create(:apportionment_account)
  end

  it "se encontrar configuracao para todos os parametros deve retornar 50%" do
    apport = Apportionment.get_apportionment(1, 1, 1, 1)
    expect(apport.dentist_percentage).to eq 50.0
  end

  it "se encontrar configuracao para todos os parametros menos para o tipo de procedimento deve retornar 70%" do
    apport = Apportionment.get_apportionment(1, 1, 1, nil)
    expect(apport.dentist_percentage).to eq 70.0
  end

  it "se encontrar configuracao para todos os parametros menos para o dentista deve retornar 40%" do
    apport = Apportionment.get_apportionment(1, 1, nil, 2)
    expect(apport.dentist_percentage).to eq 40.0
  end

  it "se encontrar configuracao apenas para conta deve retornar 30%" do
    apport = Apportionment.get_apportionment(1, 2, nil, nil)
    expect(apport.dentist_percentage).to eq 30.0
  end
end

require 'spec_helper'

describe FinancialRelease do
  before :each do
    @account = FactoryGirl.create(:account)
    @dentist = FactoryGirl.create(:dentist)
    @procedure = FactoryGirl.create(:type_procedure)

    #FactoryGirl.create(:apportionment_dentist, account_id: @account.id)
    #FactoryGirl.create(:apportionment_type_procedure, account_id: @account.id)
    #FactoryGirl.create(:apportionment_account, account_id: @account.id)
  end
  it "recebimento de valor de procedimento com % pre-definido para o dentista" do
    FactoryGirl.create(:apportionment_all, account_id: @account.id, dentist_id: @dentist.id, type_procedure_id: @procedure.id)
    release = FactoryGirl.create(:financial_release, dentist_id: @dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: @account.id, release_date: Time.now.to_date, release_type: 3)
    expect(release.dentist_value).to eq 50.0
  end

  it "recebimento de valor de procedimento sem % pre-definido para o dentista" do
    FactoryGirl.create(:apportionment_all, account_id: @account.id, dentist_id: @dentist.id, type_procedure_id: @procedure.id, dentist_percentage: 0)
    release = FactoryGirl.create(:financial_release, dentist_id: @dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: @account.id, release_date: Time.now.to_date, release_type: 3)
    expect(release.clinic_value).to eq 100.0
  end

  it "recebimento de procedimento com % configurado para dentista/procedimento" do
    FactoryGirl.create(:apportionment_all, account_id: @account.id, dentist_id: @dentist.id, type_procedure_id: @procedure.id)
    release = FactoryGirl.create(:financial_release, dentist_id: @dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: @account.id, release_date: Time.now.to_date, release_type: 3)
    expect(release.dentist_value).to eq 50.0
  end

  it "recebimento de procedimento com % configurado para dentista" do
    FactoryGirl.create(:apportionment, account_id: @account.id, dentist_id: @dentist.id, type_procedure_id: nil, dentist_percentage: 75)
    release = FactoryGirl.create(:financial_release, dentist_id: @dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: @account.id, release_date: Time.now.to_date, release_type: 3)
    expect(release.dentist_value).to eq 75.0
  end

  it "recebimento de procedimento com % configurado para procedimento" do
    FactoryGirl.create(:apportionment, account_id: @account.id, dentist_id: nil, type_procedure_id: @procedure.id, dentist_percentage: 60)
    release = FactoryGirl.create(:financial_release, dentist_id: @dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: @account.id, release_date: Time.now.to_date, release_type: 3)
    expect(release.dentist_value).to eq 60.0
  end

  it "recebimento de procedimento com % configurado apenas para conta" do
    FactoryGirl.create(:apportionment, account_id: @account.id, dentist_id: nil, type_procedure_id: nil, dentist_percentage: 40)
    release = FactoryGirl.create(:financial_release, dentist_id: @dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: @account.id, release_date: Time.now.to_date, release_type: 3)
    expect(release.dentist_value).to eq 40.0
  end

  it "saldo do detista atualizado automaticamente apos o recebimento" do
    account = FactoryGirl.create(:account)
    dentist = FactoryGirl.create(:dentist)
    FactoryGirl.create(:apportionment_account, account_id: account.id, dentist_percentage: 75.0)
    release = FactoryGirl.create(:financial_release, dentist_id: dentist.id, type_procedure_id: @procedure.id, original_value: 100.00, account_id: account.id, release_date: Time.now.to_date, release_type: 3)
    dentist_account = Account.find(dentist.account_id)
    expect(dentist.account.balance).to eq 75
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :account do
    name 'Conta principal'
    account_type 1
    organization_id 1
  end
end

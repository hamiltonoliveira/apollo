# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dentist do
    name 'Dentista 1'
    organization_id 1
  end
end

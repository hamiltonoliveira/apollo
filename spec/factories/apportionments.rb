# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :apportionment do
    organization_id 1
    account_id 1
    factory :apportionment_all do
      type_procedure_id 1
      dentist_id 1
      dentist_percentage 50
    end
    factory :apportionment_dentist do
      type_procedure_id nil
      dentist_id 1
      dentist_percentage 70
    end
    factory :apportionment_type_procedure do
      type_procedure_id 2
      dentist_id nil
      dentist_percentage 40
    end
    factory :apportionment_account do
      account_id 2
      type_procedure_id nil
      dentist_id nil
      dentist_percentage 30
    end
    factory :apportionment_no_percentage do
      account_id 3
      dentist_percentage 0
    end
  end
end

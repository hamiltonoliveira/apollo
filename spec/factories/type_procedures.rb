# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :type_procedure do
    name "procedure"
    organization_id 1
  end
end

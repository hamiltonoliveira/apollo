# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :financial_release do
    organization_id 1
    account_id 1
    description "test"
  end
end

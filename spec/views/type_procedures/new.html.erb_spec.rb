require 'spec_helper'

describe "type_procedures/new" do
  before(:each) do
    assign(:type_procedure, stub_model(TypeProcedure).as_new_record)
  end

  it "renders new type_procedure form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", type_procedures_path, "post" do
    end
  end
end

require 'spec_helper'

describe "type_procedures/edit" do
  before(:each) do
    @type_procedure = assign(:type_procedure, stub_model(TypeProcedure))
  end

  it "renders the edit type_procedure form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", type_procedure_path(@type_procedure), "post" do
    end
  end
end

require 'spec_helper'

describe "financial_releases/new" do
  before(:each) do
    assign(:financial_release, stub_model(FinancialRelease).as_new_record)
  end

  it "renders new financial_release form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", financial_releases_path, "post" do
    end
  end
end

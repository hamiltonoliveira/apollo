require 'spec_helper'

describe "financial_releases/edit" do
  before(:each) do
    @financial_release = assign(:financial_release, stub_model(FinancialRelease))
  end

  it "renders the edit financial_release form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", financial_release_path(@financial_release), "post" do
    end
  end
end

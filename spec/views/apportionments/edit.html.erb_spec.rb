require 'spec_helper'

describe "apportionments/edit" do
  before(:each) do
    @apportionment = assign(:apportionment, stub_model(Apportionment))
  end

  it "renders the edit apportionment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", apportionment_path(@apportionment), "post" do
    end
  end
end

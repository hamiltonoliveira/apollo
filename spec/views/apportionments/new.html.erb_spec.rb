require 'spec_helper'

describe "apportionments/new" do
  before(:each) do
    assign(:apportionment, stub_model(Apportionment).as_new_record)
  end

  it "renders new apportionment form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", apportionments_path, "post" do
    end
  end
end

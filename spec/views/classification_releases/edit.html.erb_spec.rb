require 'spec_helper'

describe "classification_releases/edit" do
  before(:each) do
    @classification_release = assign(:classification_release, stub_model(ClassificationRelease))
  end

  it "renders the edit classification_release form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", classification_release_path(@classification_release), "post" do
    end
  end
end

require 'spec_helper'

describe "classification_releases/new" do
  before(:each) do
    assign(:classification_release, stub_model(ClassificationRelease).as_new_record)
  end

  it "renders new classification_release form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", classification_releases_path, "post" do
    end
  end
end

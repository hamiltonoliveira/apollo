require 'spec_helper'

describe "dentists/new" do
  before(:each) do
    assign(:dentist, stub_model(Dentist).as_new_record)
  end

  it "renders new dentist form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", dentists_path, "post" do
    end
  end
end

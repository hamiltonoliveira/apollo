require 'spec_helper'

describe "dentists/edit" do
  before(:each) do
    @dentist = assign(:dentist, stub_model(Dentist))
  end

  it "renders the edit dentist form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", dentist_path(@dentist), "post" do
    end
  end
end

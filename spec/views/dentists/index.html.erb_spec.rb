require 'spec_helper'

describe "dentists/index" do
  before(:each) do
    assign(:dentists, [
      stub_model(Dentist),
      stub_model(Dentist)
    ])
  end

  it "renders a list of dentists" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
  end
end

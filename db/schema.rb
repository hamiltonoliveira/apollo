# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140507004148) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: true do |t|
    t.integer  "organization_id",               null: false
    t.string   "name",                          null: false
    t.decimal  "balance",         default: 0.0, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_type"
  end

  create_table "apportionments", force: true do |t|
    t.integer  "organization_id",    null: false
    t.integer  "account_id",         null: false
    t.integer  "dentist_id"
    t.integer  "type_procedure_id"
    t.decimal  "dentist_percentage"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "classification_releases", force: true do |t|
    t.integer  "organization_id",     null: false
    t.integer  "classification_type", null: false
    t.string   "name",                null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dentists", force: true do |t|
    t.integer  "organization_id", null: false
    t.string   "name",            null: false
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "account_id"
  end

  create_table "financial_releases", force: true do |t|
    t.integer  "account_id",                                null: false
    t.integer  "organization_id",                           null: false
    t.integer  "dentist_id"
    t.integer  "type_procedure_id"
    t.integer  "release_type",                              null: false
    t.date     "release_date",                              null: false
    t.decimal  "original_value",                            null: false
    t.decimal  "dentist_value",                             null: false
    t.decimal  "clinic_value",                              null: false
    t.boolean  "canceled",                  default: false, null: false
    t.text     "description",                               null: false
    t.text     "description_canceled"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "transferred",               default: false, null: false
    t.integer  "classification_release_id"
  end

  create_table "organizations", force: true do |t|
    t.string   "name",       limit: 100, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "type_procedures", force: true do |t|
    t.integer  "organization_id", null: false
    t.string   "name",            null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "organization_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end

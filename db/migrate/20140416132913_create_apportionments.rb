class CreateApportionments < ActiveRecord::Migration
  def change
    create_table :apportionments do |t|
      t.integer    :organization_id, null: false
      t.integer    :account_id, null: false
      t.integer    :dentist_id
      t.integer    :type_procedure_id
      t.decimal    :dentist_percentage
      t.timestamps
    end
  end
end

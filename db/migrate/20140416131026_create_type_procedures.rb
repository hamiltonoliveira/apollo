class CreateTypeProcedures < ActiveRecord::Migration
  def change
    create_table :type_procedures do |t|
      t.integer     :organization_id, null: false
      t.string      :name, null: false
      t.timestamps
    end
  end
end

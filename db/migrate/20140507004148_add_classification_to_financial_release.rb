class AddClassificationToFinancialRelease < ActiveRecord::Migration
  def change
    add_column :financial_releases, :classification_release_id, :integer
  end
end

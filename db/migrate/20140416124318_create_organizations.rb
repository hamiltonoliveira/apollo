class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|
      t.string     :name, limit: 100, null: false
      t.timestamps
    end
    
    add_column :users, :organization_id, :integer
  end
end
class AddFlagTranferToFinancialRelease < ActiveRecord::Migration
  def change
    add_column :financial_releases, :transferred, :boolean, null: false, default: false
  end
end
class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer    :organization_id, null: false
      t.string     :name, null: false
      t.decimal    :balance, null: false, default: 0
      t.timestamps
    end
  end
end

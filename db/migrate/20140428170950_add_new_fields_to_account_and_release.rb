class AddNewFieldsToAccountAndRelease < ActiveRecord::Migration
  def change
    add_column :accounts, :account_type, :integer
    add_column :dentists, :account_id, :integer
  end
end
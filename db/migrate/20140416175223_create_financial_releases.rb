class CreateFinancialReleases < ActiveRecord::Migration
  def change
    create_table :financial_releases do |t|
      t.integer     :account_id, null: false
      t.integer     :organization_id, null: false
      t.integer     :dentist_id
      t.integer     :type_procedure_id
      t.integer     :release_type, null:false #1-credito 2-debito
      t.date        :release_date, null: false
      t.decimal     :original_value, null: false
      t.decimal     :dentist_value, null:false
      t.decimal     :clinic_value, null: false
      t.boolean     :canceled, null: false, default: false
      t.text        :description, null: false
      t.text        :description_canceled
      t.timestamps
    end
  end
end

class CreateClassificationReleases < ActiveRecord::Migration
  def change
    create_table :classification_releases do |t|
      t.integer    :organization_id, null: false
      t.integer    :classification_type, null: false
      t.string     :name, null: false
      t.timestamps
    end
  end
end

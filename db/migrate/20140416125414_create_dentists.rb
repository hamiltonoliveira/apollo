class CreateDentists < ActiveRecord::Migration
  def change
    create_table :dentists do |t|
      t.integer     :organization_id, null: false
      t.string      :name, null: false
      t.integer     :user_id
      t.timestamps
    end
  end
end

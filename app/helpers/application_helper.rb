module ApplicationHelper

  def present(object, klass = nil)
    klass ||= "#{object.class}Presenter".constantize
    presenter = klass.new(object, self)
    yield presenter if block_given?
    presenter
  end

  def organization_users
    User.where(organization_id: current_user.organization_id).order("email").select("id, email")
  end

  def accounts_by_organization
    Account.where(organization_id: current_user.organization_id, account_type: 1).order("name").select("id, name")
  end

  def accounts_dentists_by_organization
    Account.where(organization_id: current_user.organization_id, account_type: 2).order("name").select("id, name, balance")
  end

  def dentists_by_organization
    Dentist.where(organization_id: current_user.organization_id).order("name").select("id, name")
  end

  def type_procedures_by_organization
    TypeProcedure.where(organization_id: current_user.organization_id).order("name").select("id, name")
  end

  def classification_by_organization_and_type(type)
    ClassificationRelease.where("organization_id = ? and classification_type = ?", current_user.organization_id, type).order("name").select("id, name")
  end
end

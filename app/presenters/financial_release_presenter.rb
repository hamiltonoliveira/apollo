class FinancialReleasePresenter
  def initialize(financial_release, template)
    @financial_release = financial_release
    @template = template
  end

  def description
    @financial_release.description
  end

  def account_name
    @financial_release.account.name if @financial_release.account_id
  end

  def dentist_name
    @financial_release.dentist_id.nil? ? "" : @financial_release.dentist.name
  end

  def type_procedure_name
    @financial_release.type_procedure_id.nil? ? "" : @financial_release.type_procedure.name
  end

  def type_release
    case @financial_release.release_type
    when FinancialRelease::CREDIT
      "Crédito Simples"
    when FinancialRelease::DEBT
      "Débito Simples"
    when FinancialRelease::RECEIVING
      "Recebimento"
    when FinancialRelease::PAYMENT
      "Pagamentos"
    when FinancialRelease::PROCEDURE_RECEIVING
      "Repasse"
    end
  end

  def date_formatted
    I18n.l @financial_release.release_date, format: :default
  end

  def value_formatted
    helpers.number_with_precision(@financial_release.original_value)
  end

  private
  def helpers
    ApplicationController.helpers
  end
end

class ClassificationReleasePresenter
  def initialize(classification_release, template)
    @classification_release = classification_release
    @template = template
  end
  
  def name
    @classification_release.name
  end
  
  def classification_type
    case @classification_release.classification_type
    when ClassificationRelease::CREDIT
      "Crédito"
    when ClassificationRelease::DEBT
      "Débito"
    end
  end
end

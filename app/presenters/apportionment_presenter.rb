class ApportionmentPresenter
  def initialize(apportionment, template)
    @apportionment = apportionment
    @template = template
  end
  
  def account_name
    @apportionment.account.name if @apportionment.account_id
  end
  
  def dentist_name
    @apportionment.dentist_id.nil? ? "Todos os dentistas" : @apportionment.dentist.name
  end
  
  def type_procedure_name
    @apportionment.type_procedure_id.nil? ? "Todos os procedimentos" : @apportionment.type_procedure.name
  end
  
end
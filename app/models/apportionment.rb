class Apportionment < ActiveRecord::Base
  default_scope order("account_id ASC")
  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }

  belongs_to :account
  belongs_to :dentist
  belongs_to :type_procedure

  validates_presence_of :account_id

  def self.get_apportionment(organization_id, account_id, dentist_id, type_procedure_id)
    apport = nil
    apport = Apportionment.where("organization_id =? and account_id = ? and dentist_id = ? and type_procedure_id = ?", organization_id, account_id, dentist_id, type_procedure_id).first
    unless apport
      apport = Apportionment.where("organization_id =? and account_id = ? and dentist_id = ? and type_procedure_id is null", organization_id, account_id, dentist_id).first
    end
    unless apport
      apport = Apportionment.where("organization_id =? and account_id = ? and type_procedure_id = ? and dentist_id is null", organization_id, account_id, type_procedure_id).first
    end
    unless apport
      apport = Apportionment.where("organization_id =? and account_id = ? and dentist_id is null and type_procedure_id is null", organization_id, account_id).first
    end
    apport
  end
end

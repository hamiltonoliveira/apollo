class Account < ActiveRecord::Base
  include I18n::Alchemy
  localize :balance, :using => :number
  before_create :set_account_type
  default_scope order("name ASC")
  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }

  validates_presence_of :name

  def self.update_balance(account_id, value)
    account = Account.where("id = ?", account_id).first
    account.balance += value
    account.save
  end

  def set_account_type
    self.account_type = 1 if self.account_type.nil?
  end
end

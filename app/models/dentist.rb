class Dentist < ActiveRecord::Base
  after_create :create_account
  belongs_to :user
  belongs_to :account

  default_scope order("name ASC")
  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }

  validates_presence_of :name
  #colocar validação para associação única com usuário


  def create_account
    account = Account.new
    account.organization_id = self.organization_id
    account.name = "Conta do(a) dentista #{self.name}"
    account.account_type = 2 #dentist
    account.save
    self.account_id = account.id
    self.save
  end
end

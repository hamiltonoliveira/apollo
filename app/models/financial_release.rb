class FinancialRelease < ActiveRecord::Base
  include I18n::Alchemy

  CREDIT = 1
  DEBT = 2
  RECEIVING = 3
  PAYMENT = 4
  PROCEDURE_RECEIVING = 5


  belongs_to :account
  belongs_to :dentist
  belongs_to :type_procedure
  belongs_to :classification_release

  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }
  scope :by_account, lambda { |account_id| where(account_id: account_id) }

  validates_presence_of :account_id
  validates_presence_of :organization_id
  validates_presence_of :release_date
  validates_presence_of :original_value
  #validates_presence_of :dentist_value
  #validates_presence_of :clinic_value
  validates_presence_of :description
  validates_presence_of :release_type

  before_create :apply_apportionments
  after_create  :update_account_balance, :verify_dentist_transfer
  before_update :verify_canceled

  localize :original_value, :using => :number
  localize :release_date, :using => :date

  def apply_apportionments
    #Colocar erro se for criar um lançamento já cancelado
    apport = Apportionment.get_apportionment(self.organization_id, self.account_id, self.dentist_id, self.type_procedure_id)
    if apport && self.release_type == RECEIVING
      self.dentist_value = self.original_value*(apport.dentist_percentage/100.00)
      self.clinic_value = self.original_value*(1 - (apport.dentist_percentage/100.00))
      self.transferred = true if self.dentist_value > 0.00
    else
      self.dentist_value = 0
      self.clinic_value = self.original_value
    end
    #Account.update_balance(self.account_id, self.original_value)
  end

  def update_account_balance
    value = self.original_value
    value = value*(-1) if (self.release_type == DEBT || self.release_type == PAYMENT)
    Account.update_balance(self.account_id, value)
  end

  def verify_canceled
    if self.canceled
      Account.update_balance(self.account_id, self.original_value*(-1))
    end
  end

  def verify_dentist_transfer
    if self.release_type == RECEIVING && self.transferred
      FinancialRelease.create({
        organization_id: self.organization_id,
        release_date: self.release_date,
        release_type: DEBT,
        account_id: self.account_id,
        original_value: self.dentist_value,
        description: "Transf. do percentual do dentista sobre o procedimento. Dentista: #{self.dentist.name}"
      })
      FinancialRelease.create({
        organization_id: self.organization_id,
        release_date: self.release_date,
        release_type: PROCEDURE_RECEIVING,
        account_id: self.dentist.account_id,
        original_value: self.dentist_value,
        description: "Recebimento do percentual do dentista sobre o procedimento. Dentista: #{self.dentist.name}"
      })
    end
  end

  def self.account_statement(account, start_date, end_date)
    prior_balance = FinancialRelease.where("account_id = ? and release_date < ? and release_type in (1,3,5)", account, start_date).sum("original_value") - FinancialRelease.where("account_id = ? and release_date < ? and release_type in (2,4)", account, start_date).sum("original_value")
    releases = FinancialRelease.where("account_id = ? and release_date between ? and ? ", account, start_date, end_date)
    final_balance = prior_balance + FinancialRelease.where("account_id = ? and release_date between ? and ? and release_type in (1,3,5)", account, start_date, end_date).sum("original_value") - FinancialRelease.where("account_id = ? and release_date between ? and ? and release_type in (2,4)", account, start_date, end_date).sum("original_value")
    return prior_balance, releases, final_balance
  end

  def self.not_transfer_values(organization_id)
    FinancialRelease.where("financial_releases.organization_id = ? and transferred = false", organization_id).joins(:dentist).select("dentists.name, dentists.id, sum(original_value) as total_value").group("dentists.id, name")
  end

  def self.transfer_dentist_value(id, value)
    fr = FinancialRelease.find(id)
    fr.dentist_value = value.to_f
    fr.clinic_value = fr.original_value - value.to_f
    fr.transferred = true
    fr.save

    FinancialRelease.create({
      organization_id: fr.organization_id,
      release_date: Time.now.to_date,
      release_type: DEBT,
      account_id: fr.account_id,
      original_value: fr.dentist_value,
      description: %Q[
        Transf. do percentual do dentista sobre o procedimento.
        Dentista: #{fr.dentist.name}
        #{fr.description}
        ]
    })
    FinancialRelease.create({
      organization_id: fr.organization_id,
      release_date: Time.now.to_date,
      release_type: PROCEDURE_RECEIVING,
      account_id: fr.dentist.account_id,
      original_value: fr.dentist_value,
      description: %Q[
        Recebimento do percentual do dentista sobre o procedimento.
        Dentista: #{fr.dentist.name}
        #{fr.description}
        ]
    })

  end

end

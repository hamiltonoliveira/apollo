class TypeProcedure < ActiveRecord::Base
  default_scope order("name ASC")
  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }
  
  validates_presence_of :name
end

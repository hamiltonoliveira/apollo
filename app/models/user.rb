class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :verify_organization
  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }
  def verify_organization
    if self.organization_id.nil?
      org = Organization.create(name: "Colocar o nome da empresa/consultório aqui")
      self.organization_id = org.id
    end
  end
end

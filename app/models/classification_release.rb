class ClassificationRelease < ActiveRecord::Base
  CREDIT = 1
  DEBT = 2
  default_scope order("name ASC")
  scope :by_organization, lambda { |organization_id| where(organization_id: organization_id) }
  
  validates_presence_of :name
  validates_presence_of :classification_type
end

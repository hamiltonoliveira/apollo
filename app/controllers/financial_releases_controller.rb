class FinancialReleasesController < InheritedResources::Base
  #actions :all, :except => [:show]

  has_scope :by_organization
  has_scope :by_account

  def collection
    @financial_releases ||= end_of_association_chain.paginate(:page => params[:page])
  end

  def new
    @type_release = params[:type_release]
    new!
  end

  def create
    @financial_release = FinancialRelease.new
    @financial_release.localized.assign_attributes(permitted_params[:financial_release])
    @financial_release.organization_id = current_user.organization_id
    create! do |format|
      format.html { redirect_to :back, notice: "Lançamento salvo com sucesso." }
    end
  end

  def payment
    @financial_release = FinancialRelease.new
    @account_id = params[:account_id]
  end

  def transfer_values
    @values = FinancialRelease.not_transfer_values(current_user.organization_id)
  end

  def dentist_open_values
    @financial_releases = FinancialRelease.where("dentist_id = ? and transferred = false", params[:dentist_id])
  end

  def update_dentist_values
    params.each do |param, value|
      if param.include?('value_')
        id =  param[6..-1]
        FinancialRelease.transfer_dentist_value(id, value)
      end
    end
    redirect_to root_path
  end

  def permitted_params
    params.permit(:financial_release => [:description, :account_id, :release_date, :original_value, :dentist_id, :release_type, :description_canceled, :canceled, :type_procedure_id, :classification_release_id])
  end
end

class UsersController < InheritedResources::Base
  actions :all, :except => [:show]
  has_scope :by_organization
end

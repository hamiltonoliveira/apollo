class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :authenticate_user!, :set_scope_organization_in_params
  
  def set_scope_organization_in_params
    params[:by_organization] = current_user.id if current_user
  end
end

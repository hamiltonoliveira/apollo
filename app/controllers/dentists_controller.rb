class DentistsController < InheritedResources::Base
  actions :all, :except => [:show]
  
  has_scope :by_organization
  
  def collection
    @dentists ||= end_of_association_chain.paginate(:page => params[:page])
  end
  
  def permitted_params
    params.permit(:dentist => [:name, :user_id])
  end
  
end

class TypeProceduresController < InheritedResources::Base
  actions :all, :except => [:show]
  
  has_scope :by_organization
  
  def collection
    @type_procedures ||= end_of_association_chain.paginate(:page => params[:page])
  end
  
  def permitted_params
    params.permit(:type_procedure => [:name, :user_id])
  end
end

class ApportionmentsController < InheritedResources::Base
  actions :all, :except => [:show]
  
  has_scope :by_organization
  
  def collection
    @apportionments ||= end_of_association_chain.paginate(:page => params[:page])
  end
  
  def permitted_params
    params.permit(:apportionment => [:account_id, :dentist_id, :type_procedure_id, :dentist_percentage])
  end
end

class HomeController < ApplicationController
  def index
    @accounts = Account.where("organization_id = ? and account_type = 2", current_user.organization_id)
  end
end

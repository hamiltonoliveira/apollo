class AccountsController < InheritedResources::Base
  actions :all, :except => [:show]
  
  has_scope :by_organization
  
  def collection
    @accounts ||= end_of_association_chain.paginate(:page => params[:page])
  end
  
  def permitted_params
    params.permit(:account => [:name])
  end
end

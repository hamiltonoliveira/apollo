class ClassificationReleasesController < InheritedResources::Base
  actions :all, :except => [:show]
  
  has_scope :by_organization
  
  def collection
    @classification_releases ||= end_of_association_chain.paginate(:page => params[:page])
  end
  
  def permitted_params
    params.permit(:classification_release => [:name, :classification_type])
  end
end

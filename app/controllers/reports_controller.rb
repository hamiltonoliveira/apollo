class ReportsController < ApplicationController

  def account_statement
    @releases = []
    if params[:account] && params[:start_date] && params[:end_date]
      start_date = DateTime.strptime(params[:start_date], "%d/%m/%Y")
      end_date = DateTime.strptime(params[:end_date], "%d/%m/%Y")
      @prior_balance, @releases, @final_balance = FinancialRelease.account_statement(params[:account], start_date, end_date)
    end
  end

  def dentist_statement
    @releases = []
    if params[:account] && params[:start_date] && params[:end_date]
      start_date = DateTime.strptime(params[:start_date], "%d/%m/%Y")
      end_date = DateTime.strptime(params[:end_date], "%d/%m/%Y")
      @prior_balance, @releases, @final_balance = FinancialRelease.account_statement(params[:account], start_date, end_date)
    end
  end

  def receipt
    @financial_release = FinancialRelease.find(params[:id])
    @dentist = Dentist.where("account_id = ?", @financial_release.account_id)
    respond_to do |format|
      format.pdf do
        render :pdf => "recibo", :template => "reports/receipt.pdf.erb"
      end
    end
  end
end

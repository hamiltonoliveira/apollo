set :application, 'apollo'
set :scm, :git
set :repo_url, 'https://hamiltonoliveira@bitbucket.org/hamiltonoliveira/apollo.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :deploy_to, '/home/deploy/apps/apollo'
# set :scm, :git

set :format, :pretty
# set :log_level, :debug
set :pty, true

# set :linked_files, %w{config/database.yml}
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc "Copy database.yml"
  task :copy_database_yml do
    on "107.170.189.237" do
      execute "cp -f /home/deploy/database.yml.production #{release_path}/config/database.yml"
    end
  end

  before "deploy:assets:precompile", "deploy:copy_database_yml"
  after :finishing, 'deploy:cleanup'

end
